# Information #

SwitchButtonField is an addon UI component for use within a SilverStripe 3.X CMS interface. It's based upon the jQuery [SwitchButton](https://github.com/olance/jQuery-switchButton) library, written by [Olivier Lance](https://github.com/olance/)

***

## Examples ##

Examples of the SwitchButton functionality and options can be found [here](http://olance.github.io/jQuery-switchButton/).

## Installation ##
### Composer ###
### GIT ###
### Manual ###

## Usage ##
### Creating a new field ###
    SwitchButtonField::create($name, $title, $value, $options=array());
Similar to the way in which most SilverStripe fields are built, the only notable difference being the `$options` array, in which you specify the settings for display. The options you can make use of are as follows:

| Option | Description |
|--------|-------------|
| Something | Description |