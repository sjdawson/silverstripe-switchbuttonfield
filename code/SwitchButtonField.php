<?php

/**
 * SwitchButtonField
 *
 * SwitchButtonField allows the CMS user to switch between on and off states, with
 * more feedback as to what the outcome would be and less cumbersome than a checkbox
 * to click on. Written for a need to help someone with arthritis
 *
 * Utilisies the jQuery version of the library "jQuery-switchButton"
 * https://github.com/olance/jQuery-switchButton
 *
 * @author Steven Dawson <steven@djdawson.co.uk>
 *
 * @todo Refactor the template to remove inline script and replace with .data attributes
 *
 */
class SwitchButtonField extends CheckboxField
{
    /**
     * A range of options that the field will use to be displayed
     */
    protected $on_label, $off_label;



    /**
     * Construct the field based on the options given
     */
    public function __construct($name, $title = null, $value = null, $on_label = "ON", $off_label = "OFF")
    {
        parent::__construct($name, $title, $value, $on_label, $off_label);

        $this->on_label = $on_label;
        $this->off_label = $off_label;
    }



    /**
     * Add any additional scripts and stylesheets we may need to the admin section when called
     */
    public function requirements()
    {
        // Javascript
        Requirements::set_write_js_to_body(true);
        Requirements::javascript("SwitchButtonField/js/jquery.switchButton.js");
        Requirements::set_write_js_to_body(false);

        // CSS
        Requirements::css("SwitchButtonField/css/jquery.switchButton.css");
    }



    /**
     * Scaffold the field in the usual SilverStripe fashion
     */
    public function Field($properties = array())
    {
        $this->requirements();
        $obj = ($properties) ? $this->customise($properties) : $this;

        return $obj->renderWith($this->getTemplates());
    }
}
